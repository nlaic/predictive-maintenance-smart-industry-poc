# Predictive Maintenance Smart Industry Proof of Concept

This repostory contains the deployment configuration of the TSG Core Container, TSG OpenAPI Data App, and Eclipse BaSyx services. This can be used to share Asset Administration Shell data across parties in the dataspace.

The figure below shows the overview of how this deployment could be deployed at an manufacturing company. This repository contains the upper part of the figure, the lower part (PLM, MES, ERP) should be configured on a per company basis. A similar setup can be envisioned for an equipment maintenance company, although the lower part will be different in that case.

![Overview](figures/overview.drawio.svg)

## Goal

The goal of this Proof of Concept is to enable secure data sharing of factory data in order to improve the equipment utilization by taking smarter decisions on when to execute maintenance. Both the manufacturing company and the equipment maintenance company have sensitive data that when combined in a secure way can improve the algorithms regarding maintaining the equipment on the factory floor.

## Usage

The steps below show the steps that should be executed to register at a dataspace and install the connector on a Kubernetes cluster.

### Requirements
- Kubernetes cluster
  - CLIs: kubectl, helm
  - GUI: OpenLens (optional)
- Nginx Ingress Controller
  - Including Cert Manager
  - A configured domain with DNS entries towards the ingress controller

For more information see the [TSG Deployment Documentation](https://tno-tsg.gitlab.io/docs/deployment/kubernetes/).

### 1. Register at a dataspace

Register at a dataspace in order to receive a certificate to communicate with an DAPS. For example, the [TSG Playground](https://daps.playground.dataspac.es/) can be used as test environment for the first tests.

> For production deployments, a real dataspace should be chosen with an well established governance model.

### 2. Configure the connector

In the previous step you should have gathered your certificate, private key, and a CA chain. For the TSG Core Container, these should be in the PKCS#8 PEM format (files starting with `-----BEGIN CERTIFICATE-----` or `-----BEGIN PRIVATE KEY-----`).

Your folder should have a structure similar to:
```
.
├── values.yaml
├── component.key
├── component.crt
└── cachain.crt
```

The certificate details can now be uploaded to your Kubernetes cluster via a Secret: 

```shell
# Kubernetes namespace
export namespace=nlaic-pmsi
# Helm deployment name
export deployment=nlaic-pmsi

kubectl create namespace $namespace

kubectl create secret generic \
    -n $namespace \
    ids-identity-secret \
    --from-file=ids.crt=./component.crt \
    --from-file=ids.key=./component.key \
    --from-file=ca.crt=./cachain.crt
```

After that, the `values.yaml` file should be updated according to your environment. In the file all lines annotated with `# CHANGE` should be changed. A list of the changes that should be made:
- Line 2 (`host`): the domain name under which the connector will be available, the DNS of this domain should point to the Ingress controller.
- Line 46 (`ids.info.idsid`): the IDS component identifier of your certificate.
- Line 48 (`ids.info.curator`): the IDS participant identifier linked to your certificate.
- Line 50 (`ids.info.maintainer`): the IDS participant identifier linked to your certificate.
- Line 53 (`ids.info.titles[0]`): the title of your connector.
- Line 56 (`ids.info.descriptions[0]`): the description of your connector.
- Line 66 (`ids.security.apiKeys[0].key`): the API key used by the OpenAPI data app to communicate with the Core Container.
- Line 66 (`ids.security.users[0].password`): the password of the admin user for interaction with the Core Container via the UI.
- Line 114 (`containers[0].apiKey`): the API key used by the OpenAPI data app to communicate with the Core Container. _Should match with line 66_.
- Line 126 (`containers[0].config.openApi.agents[0].id`): Identifier for the agent that will serve the BaSyx information.


### 3. Deploy the connector

To deploy the connector the TSG Helm repository should be added and the `tsg-connector` Helm chart should be installed.

```
helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm


helm upgrade --install -n $namespace $deployment tsg/tsg-connector --version 3.1.1-master -f values.yaml
```
### 3.1. Upgrade the connector

For upgrading the connector the same command as for installing the connector can be used: 
```
helm upgrade --install -n aic-pef-a aic-pef-a tsg/tsg-connector --version 3.1.1-master -f values.yaml
```

In most cases it is useful to use the [Helm diff plugin](https://github.com/databus23/helm-diff) to quickly see whether the changes you made to the `values.yaml` file are reflected correctly in the deployment.

```
helm diff upgrade --install -n aic-pef-a aic-pef-a tsg/tsg-connector --version 3.1.1-master -f values.yaml
```

### 4. Connect existing services

The most difficult step would be to connect existing services to the BaSyx service. When these services are also located within the same Kubernetes cluster, the internal ClusterIP service can be used to communicate with BaSyx. If these services are outside of the cluster the BaSyx service should be exposed, e.g. via an Ingress. The OpenAPI Data App is exposed in a similar way, and this configuration can be copied to the service of BaSyx in the `values.yaml`.

## Additional resources

* TSG Documentation: https://tno-tsg.gitlab.io/
* TSG Gitlab Group: https://gitlab.com/tno-tsg/
* Eclipse BaSyx: https://www.eclipse.org/basyx/
* Plattform I4.0 / Asset Administration Shell: https://www.plattform-i40.de/IP/Navigation/EN/Home/home.html

